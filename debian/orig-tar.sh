#!/bin/sh -e
#
# Removes unwanted content from the upstream sources.
# Called by uscan with '--upstream-version' <version> <file>
#

VERSION=$2
PACKAGE=$(dpkg-parsechangelog -S Source)
TAR=../${PACKAGE}_${VERSION}.orig.tar.xz
DIR=${PACKAGE}-${VERSION}
TAG=$(echo "geronimo-j2ee-management_1.1_spec-$VERSION" | sed -re's/~(alpha|beta|rc)/-\1/')

svn export https://svn.apache.org/repos/asf/geronimo/specs/tags/${TAG}/ $DIR
XZ_OPT=--best tar -c -v -J -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG
